package main

import (
	"bufio"
	"fmt"
	"log"
	"net"
	"os"
	"strings"
	"sync"
)

var listTask []*Task
var listWorker []*Worker
var listTaskMutex sync.Mutex
var nodeCounter = 0
var nodeCounterMutex sync.Mutex

const BUFFERSIZE = 1024

type Task struct {
	ID       string
	FileName string
	Node     string
	Status   string
	Conn     net.Conn
}

type Worker struct {
	Ip         string
	Port       string
	Passphrase string
	Token      string
}

func (s *Worker) Authenticate(conn net.Conn) bool {
	_, err := conn.Write([]byte(fmt.Sprintf("TOKEN %s", s.Token)))

	if err != nil {
		log.Fatal(err.Error())
		return false
	}

	buffer := make([]byte, BUFFERSIZE)

	_, err = conn.Read(buffer)
	if err != nil {
		log.Fatal(err.Error())
		return false
	}
	byteString := string(buffer)
	if strings.Contains(byteString, "OK") {
		return true
	}
	return false
}

func (s *Worker) Login() bool {
	conn, err := net.Dial("tcp", fmt.Sprintf("%s:%s", s.Ip, s.Port))
	defer conn.Close()
	if err != nil {
		log.Fatal(err.Error())
		return false
	}

	conn.Write([]byte(fmt.Sprintf("AUTHENTICATE %s", s.Passphrase)))

	buffer := make([]byte, BUFFERSIZE)
	n, err := conn.Read(buffer)
	if err != nil {
		log.Fatal(err.Error())
		return false
	}
	bufferRead := buffer[:n]
	stringByte := strings.Split(string(bufferRead), " ")
	if stringByte[0] == "SUCCESS" {
		s.Token = stringByte[1]
		return true
	}
	return false
}

func (s *Worker) GetAddress() string {
	return fmt.Sprintf("%s:%s", s.Ip, s.Port)
}

func GetCurrentWorker() *Worker {
	nodeCounterMutex.Lock()
	defer nodeCounterMutex.Unlock()
	counter := nodeCounter % len(listWorker)
	nodeCounter++
	worker := listWorker[counter]
	return worker
}

func sendFile(conn net.Conn, filename string, task *Task) error {
	file, err := os.Open(filename)
	if err != nil {
		panic(err)
	}
	defer file.Close()

	fileInfo, _ := file.Stat()
	conn.Write([]byte(fmt.Sprintf("FILE %d\n", fileInfo.Size())))

	accepted := false

	for !accepted {
		data, err := bufio.NewReader(conn).ReadString('\n')
		if err != nil {
			log.Fatal(err.Error())
			return err
		}
		if strings.Contains(data, "DATA") {
			data = data[:len(data)-1]
			parsedData := strings.Split(data, " ")
			task.Node = parsedData[1]
			task.ID = parsedData[2]
		}
		if task.ID != "" && task.Node != "" {
			accepted = true
		}
	}

	fileSize := int(fileInfo.Size())
	sendBuffer := make([]byte, BUFFERSIZE)
	log.Println("Start sending file!")
	chunk := fileSize / BUFFERSIZE
	if fileSize%BUFFERSIZE > 0 {
		chunk++
	}
	log.Printf("Chunk %d\n", chunk)
	for i := 0; i < chunk; i++ {
		n, err := file.Read(sendBuffer)
		if err != nil {
			log.Fatal(err.Error())
			return err
		}
		if i == chunk-1 {
			conn.Write(append(sendBuffer[:n], []byte("DONE")...))
			break
		}
		conn.Write(sendBuffer)
	}
	log.Println("Finish sending file!")
	return nil
}

func receiveFile(conn net.Conn, filename string) {
	var file, err = os.OpenFile(filename, os.O_RDWR|os.O_CREATE|os.O_TRUNC, 0755)
	if err != nil {
		panic(err)
	}
	defer file.Close()

	buffer := make([]byte, BUFFERSIZE)
	log.Println("start transfr!")
	counterReceived := 0
	counterWrite := 0
	for {
		n, err := conn.Read(buffer)
		if err != nil {
			log.Fatal(err.Error())
			return
		}
		data := buffer[:n]
		stringData := string(data)
		done := false
		if strings.Contains(stringData,"FINISHED") {
			stringData = strings.Replace(stringData,"FINISHED","",1)
			done = true
		}
		x, err := file.WriteString(stringData)
		if err != nil {
			log.Fatal(err.Error())
			return
		}
		counterReceived += n
		counterWrite += x
		if done {
			break
		}
	}
	log.Printf("Received Total %d Write Total %d\n", counterReceived, counterWrite)
	log.Println("Transfer success!")
}

func convertCsvToJson(filename string) {
	worker := GetCurrentWorker()
	conn, err := net.Dial("tcp", worker.GetAddress())
	if err != nil {
		log.Println(err)
		return
	}

	worker.Authenticate(conn)

	modFilename := filename[:len(filename)-4]

	task := &Task{
		FileName: modFilename,
		Status:   "WAITING",
		Conn:     conn,
	}

	listTaskMutex.Lock()
	listTask = append(listTask, task)
	listTaskMutex.Unlock()

	err = sendFile(conn, filename, task)
	if err != nil {
		return
	}

	doneProcessed := false

	buffer := make([]byte, BUFFERSIZE)
	for !doneProcessed {
		n, err := conn.Read(buffer)
		if err != nil {
			log.Fatal(err.Error())
			return
		}
		bufferRead := buffer[:n]
		stringByte := string(bufferRead)
		if strings.Contains(stringByte, "FILE") {
			doneProcessed = true
		} else {
			task.Status = stringByte[:len(stringByte)-1]
			if strings.Contains(stringByte, "FAILED") {
				return
			}
		}
	}

	filename = fmt.Sprintf("output_%s.json", modFilename)
	receiveFile(conn, filename)
	task.Status = "FINISHED"
	conn.Close()
	log.Printf("Task ID %s Done\n",task.ID)
}

func checkWorkerStatus() {
	if len(listWorker) == 0 {
		fmt.Println("There is no worker node to check")
		return
	}
	fmt.Println("Getting Status")
	for _, worker := range listWorker {
		address := worker.GetAddress()
		conn, err := net.Dial("tcp", address)
		if err != nil {
			log.Println(err)
			return
		}
		defer conn.Close()

		worker.Authenticate(conn)

		conn.Write([]byte("STATUS\n"))
		bs := make([]byte, BUFFERSIZE)
		_, err = conn.Read(bs)
		if err != nil {
			log.Fatalln("read messed up", err.Error())
			panic(err)
		}
		fmt.Printf("Worker %s %s", worker.GetAddress(), bs)
	}
}

func printAllTask() {
	listTaskMutex.Lock()
	defer listTaskMutex.Unlock()
	if len(listTask) == 0 {
		fmt.Println("No task yet")
		return
	}
	for _, task := range listTask {
		fmt.Printf(
			"Task ID - %s;Filename %s;Active node %s;Status %s\n",
			task.ID, task.FileName, task.Node, task.Status)
	}
}

func runCli() {
	for {
		fmt.Print(`
Select command:
* convert [filename.csv] : to convert a csv file to a json file
* status_worker: check all convert file status
* status_task  : check all convert file status
* add_worker [ip_worker] [port]	[password]: add worker
* exit         : leave the program

Input: `)
		input, _ := bufio.NewReader(os.Stdin).ReadString('\n')
		splitInput := strings.Split(input, " ")
		command := splitInput[0]
		lcommand := strings.ToLower(input[:len(input)-1])

		if strings.ToLower(command) == "convert" {
			filename := strings.Join(splitInput[1:], " ")
			filename = filename[:len(filename)-1]

			if _, err := os.Stat(filename); os.IsNotExist(err) {
				fmt.Println("ERR: File '" + filename + "' doesn't exist.")
			} else {
				if len(listWorker) == 0 {
					fmt.Println("Add a worker node first")

				}else {
					fmt.Println("Conversion will run in the background.")
					go convertCsvToJson(filename)
				}
			}
		} else if lcommand == "status_worker" {
			checkWorkerStatus()
		} else if lcommand == "status_task" {
			printAllTask()
		} else if lcommand == "exit" {
			break
		} else if command == "add_worker" {
			addWorker(splitInput[1], splitInput[2], splitInput[3])
		} else {
			fmt.Println("ERR: Command '" + lcommand + "' not found")
		}
	}
}

func addWorker(ip, port, password string) {
	newWorker := &Worker{
		Ip:         ip,
		Port:       port,
		Passphrase: password,
	}

	success := newWorker.Login()
	if success {
		listWorker = append(listWorker,newWorker)
		fmt.Printf("Success add %s with token %s\n", newWorker.GetAddress(),newWorker.Token)
	} else {
		fmt.Printf("Fail add %s\n", newWorker.GetAddress())
	}
}

func main() {
	logfile, err := os.OpenFile(
		"logfile.log",
		os.O_RDWR|os.O_CREATE|os.O_APPEND,
		0666)
	if err != nil {
		log.Fatalf("error opening file: %v", err)
	}
	defer logfile.Close()
	log.SetOutput(logfile)

	runCli()
}
