package csv

import (
	"bytes"
	"encoding/csv"
	"encoding/json"
	"log"
	"strconv"
	"strings"
)

func Convert(theCsv *bytes.Buffer) *bytes.Buffer {
	reader := csv.NewReader(theCsv)
	content, _ := reader.ReadAll()

	if len(content) < 1 {
		log.Fatal("File is empty")
	}

	var header []string
	for _, j := range content[0] {
		header = append(header, j)
	}

	content = content[1:]

	var b bytes.Buffer
	b.WriteString("[")

	for i, j := range content {
		b.WriteString("{")
		for k, l := range j {
			b.WriteString(`"` + header[k] + `":`)
			_, isNumeric := strconv.ParseFloat(l, 64)
			_, isBool := strconv.ParseBool(l)

			if isNumeric == nil {
				b.WriteString(l)
			} else if isBool == nil {
				b.WriteString(strings.ToLower(l))
			} else {
				b.WriteString(`"` + l + `"`)
			}

			if k < len(j)-1 {
				b.WriteString(",")
			}
		}
		b.WriteString("}")
		
		if i < len(content) - 1 {
			b.WriteString(",")
		} 
	}
	b.WriteString("]")

	var jsonData = json.RawMessage(b.String())
	return bytes.NewBuffer(jsonData)
}
