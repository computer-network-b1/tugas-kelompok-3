package main

import (
	"bufio"
	"bytes"
	"fmt"
	"log"
	"math/rand"
	"net"
	"os"
	"slave/csv"
	"strings"
	"sync"
	"time"
)

var globalIDCounter = 0
var globalIDMux sync.Mutex
var workerBusyMux sync.Mutex
var workerBusy = false
var nodeNumber = os.Getenv("NODE_NUMBER")
var password = os.Getenv("PASSWORD")
var tokenList = []string{}

type Task struct {
	conn net.Conn
	statusChannel chan int
	stopSignal chan bool
	successStopSignal chan bool
	stopped bool
	done bool
	status int
	data *bytes.Buffer
	id int
	err error
}

type Queue struct {
	mux sync.Mutex
	queue []*Task
}


const BUFFERSIZE = 1024
func sendFile(conn net.Conn, file *bytes.Buffer) {
	conn.Write([]byte(fmt.Sprintf("FILE %d\n", file.Len())))
	sendBuffer := make([]byte, BUFFERSIZE)
	log.Println("Start sending file!")
	chunk := file.Len() / BUFFERSIZE
	if file.Len() % BUFFERSIZE > 0 {
		chunk++
	}
	log.Printf("Chunk %d\n",chunk)
	for i:=0;i<chunk;i++ {
		n, err := file.Read(sendBuffer)
		if err != nil{
			log.Fatal(err.Error())
			return
		}
		conn.Write(sendBuffer[:n])
	}
	log.Println("Finish sending file!")
}

func receiveFile(conn net.Conn) (*bytes.Buffer,error) {
	buffer := make([]byte,BUFFERSIZE+4)
	var bufferByte bytes.Buffer
	log.Println("start transfr!")
	counterReceived := 0
	counterWrite := 0
	for {
		n,err := conn.Read(buffer)
		if err != nil {
			log.Fatal(err.Error())
			return nil,err
		}
		data := buffer[:n]
		end := false
		if b := data[n-4:]; string(b) == "DONE" {
			data = buffer[:n-4]
			end = true
		}
		x, err := bufferByte.Write(data)
		if err != nil {
			log.Fatal(err.Error())
			return nil,err
		}
		counterReceived += n
		counterWrite += x
		if end {
			break
		}
	}
	log.Printf("Received Total %d Write Total %d\n",counterReceived,counterWrite)
	log.Println("Transfer success!")
	return &bufferByte,nil
}

//SCHEDULER

//Mengeksekusi perintah yang diberikan oleh client
func Handler(conn net.Conn, queue *Queue,taskData *Task)  {
	for {
		data, err := bufio.NewReader(conn).ReadString('\n')
		if err != nil {
			if !taskData.done {
				log.Println("Client exit")
			}
			return
		}

		if strings.Contains(data, "FILE") {
			break
		}

		if strings.Contains(data, "STATUS"){
			var status string
			workerBusyMux.Lock()
			if len(queue.queue) == 0 && !workerBusy {
				status = "FREE"
			}
			if workerBusy || len(queue.queue) > 0{
				status = "EXECUTING"
			}
			workerBusyMux.Unlock()
			conn.Write([]byte(status+"\n"))
		}
		if strings.Contains(data,"STOP"){
			conn.Write([]byte("STOPPING\n"))
			taskData.stopSignal <- true
			for {
				select {
				case <-taskData.successStopSignal:
					return
				default:
					if taskData.stopped {
						return
					}
				}
				time.Sleep(100*time.Millisecond)
			}
		}
		time.Sleep(100*time.Millisecond)
	}
	globalIDMux.Lock()
	globalIDCounter++
	id := globalIDCounter
	globalIDMux.Unlock()
	taskData.statusChannel = make(chan int)
	taskData.id = id
	taskData.conn = conn
	taskData.stopSignal = make(chan bool)
	taskData.successStopSignal = make(chan bool)
	conn.Write([]byte(fmt.Sprintf("DATA %s %s%d\n",nodeNumber,nodeNumber,id)))
	data,err := receiveFile(conn)
	if err != nil {
		conn.Write([]byte("FAILED"+"\n"))
		log.Printf("Bye bye client %s\n",conn.RemoteAddr().String())
		conn.Close()
		return
	}
	taskData.data = data

	go taskData.WatchTask()
	queue.mux.Lock()
	workerBusyMux.Lock()
	if workerBusy || len(queue.queue) > 0 {
		taskData.statusChannel <- -1
	}
	workerBusyMux.Unlock()
	queue.queue = append(queue.queue,taskData)
	queue.mux.Unlock()
	Handler(conn,queue,taskData)
}


// EXECUTOR SEGMENT

//Eksekutor
func Worker(taskQueue *Queue) {
	var task *Task
	for {
		taskQueue.mux.Lock()
		if len(taskQueue.queue) != 0 {
			task = taskQueue.queue[0]
			taskQueue.queue = taskQueue.queue[1:]
			taskQueue.mux.Unlock()
			break
		}
		taskQueue.mux.Unlock()
		time.Sleep(50*time.Millisecond)
	}

	ChangeWorkerState(true)
	task.statusChannel <- 0
	DoTask(task)
	time.Sleep(100*time.Millisecond)
	if !task.stopped {
		task.statusChannel <- 1
	}
	ChangeWorkerState(false)
	Worker(taskQueue)
}

//Rubah status worker
func ChangeWorkerState(status bool){
	workerBusyMux.Lock()
	workerBusy = status
	workerBusyMux.Unlock()
}

//Fungsi task
func DoTask(task *Task) {
	CheckStopSignal(task)
	if task.stopped {
		return
	}
	data := csv.Convert(task.data)
	CheckStopSignal(task)
	if task.stopped {
		return
	}
	sendFile(task.conn,data)
}

//WATCHER SEGMENT

//Mengamati stop signal
func CheckStopSignal(task *Task){
	select {
	case <-task.stopSignal:
		task.stopped = true
		task.statusChannel <- 3
	default:
		return
	}
}

//Berfungsi untuk mengamati status dari task
func (s *Task) WatchTask() {
	defer close(s.statusChannel)
	defer close(s.successStopSignal)
	defer close(s.stopSignal)
	for {
		select {
		case status := <-s.statusChannel:
			s.status = status
			switch status {
			case -1:
				s.conn.Write([]byte("PENDING"+"\n"))
			case 0:
				s.conn.Write([]byte("RUNNING"+"\n"))
			case 1:
				s.conn.Write([]byte("FINISHED"+"\n"))
				s.done = true
				log.Printf("Bye bye client %s\n",s.conn.RemoteAddr().String())
				return
			case -2:
				s.conn.Write([]byte("FAILED"+"\n"))
				log.Printf("Bye bye client %s\n",s.conn.RemoteAddr().String())
				return
			case 3:
				s.conn.Write([]byte("STOPPED"+"\n"))
				log.Printf("Bye bye client %s\n",s.conn.RemoteAddr().String())
				s.successStopSignal <- true
				return
			}
		}
		time.Sleep(100*time.Millisecond)
	}
}

func randomString(length int) (string) {
	var output strings.Builder

	charSet := "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789"
	for i := 0; i < length; i++ {
		random := rand.Intn(len(charSet))
		randomChar := charSet[random]
		output.WriteString(string(randomChar))
	}
	return output.String()
}

func checkAuthenticate(conn net.Conn, queue *Queue, taskData *Task) {
	data, err := bufio.NewReader(conn).ReadString('\n')
	if err != nil {
		panic(err)
	}

	replacedData := strings.Replace(data, "\n", "", 1)
	replacedData = strings.Replace(replacedData, "\r", "", 1)
	splitData := strings.Split(replacedData, " ")
	command := splitData[0]

	switch command {
	case "AUTHENTICATE":
		if splitData[1] == password {
			newToken := randomString(32)
			tokenList = append(tokenList, newToken)
			conn.Write([]byte("SUCCESS " + newToken + "\n"))
			log.Printf("Client %s logged in with token %s\n",conn.RemoteAddr().String(),newToken)
		} else {
			conn.Write([]byte("FAIL WRONG PASSWORD\n"))
		}
		log.Printf("Bye bye client %s\n",conn.RemoteAddr().String())
	case "TOKEN":
		flag := false
		for _, token := range tokenList {
			if token == splitData[1] {
				conn.Write([]byte("OK\n"))
				Handler(conn, queue, taskData)
				flag = true
				break
			}
		}
		if !flag {
			conn.Write([]byte("FAIL WRONG TOKEN\n"))
		}
	}
}


func main() {
	port := os.Getenv("SLAVE_PORT")

	if port == "" {
		log.Fatal("Please provide environment variable SLAVE_PORT")
		os.Exit(1)
	}

	if nodeNumber == "" {
		log.Fatal("Please provide environment variable NODE_NUMBER")
		os.Exit(1)
	}

	if password == "" {
		log.Fatal("Please provide environment variable PASSWORD")
		os.Exit(1)
	}

	serv, err := net.Listen("tcp",fmt.Sprintf(":%s",port))
	if err != nil {
		fmt.Println("Error listening:", err.Error())
		os.Exit(1)
	}
	log.Printf("Starting TCP connection in port %s",port)
	defer serv.Close()

	queue := &Queue{
		queue: []*Task{},
	}

	go Worker(queue)

	for {
		c, err := serv.Accept()
		if err != nil {
			fmt.Println("Error connecting:", err.Error())
			return
		}
		fmt.Println("Client connected.")

		fmt.Println("Client " + c.RemoteAddr().String() + " connected.")

		task := &Task{}

		go checkAuthenticate(c,queue,task)
	}

}
