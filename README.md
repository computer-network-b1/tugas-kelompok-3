# Socket Programming

Member:

- Dipta Laksmana Baswara Dwiyantoro - 1806235832
- Ryo Axtonlie - 1806205571
- Muhammad Oktoluqman Fakhrianto - 1806186723

## Description

This is a program to convert a csv file into a json file by sending a file from
master node (master/main.go) to the worker node (slave/main.go). The master
node can check the conversion status while it is running.

## Dependencies

- docker
- docker-compose
- golang
- make

## How To Use

### Slave

1. Open `docker-compose.yml` file in the slave directory
2. Change the node `PASSWORD` and `NODE_NUMBER` variable
3. Execute command `make run-slave`
4. Let the program run to serve the master node

### Master

1. Execute command `make run-master`
2. Type the command you want to run

