BUILD_NAME=slave
FLAGS = -t $(BUILD_NAME) --no-cache
FILE_COMPOSE=slave/docker-compose.yml
DOCKER_COMPOSE=docker-compose -f
MASTER_FILE=master/main.go

build-slave: ./slave/Dockerfile ./slave/main.go
	docker build ./slave/. $(FLAGS)
	docker image prune -f

run-slave: build-slave
	$(DOCKER_COMPOSE) $(FILE_COMPOSE) up -d

stop-slave:
	$(DOCKER_COMPOSE) $(FILE_COMPOSE) stop

remove-slave: stop-slave
	$(DOCKER_COMPOSE) $(FILE_COMPOSE) rm

clean: remove-container
	docker image rm $(BUILD_NAME)

build-master:
	go build -o master master/main.go

run-master: build-master
	./master/main

